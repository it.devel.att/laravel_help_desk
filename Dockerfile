FROM php:7.4
RUN apt-get update -y && apt-get install -y openssl zip unzip git libxml2-dev
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - &&  apt-get install -y nodejs
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install mysqli pdo pdo_mysql tokenizer xml pcntl
WORKDIR /app
COPY . /app
RUN composer install
RUN npm install
RUN npm run dev
RUN rm public/storage || true
RUN php artisan storage:link

CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181
