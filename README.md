
## Laravel Help Desk demo application

* При разрабобтке используется
  * PHP 7.2.24
  * Composer version 1.10.5
  * Laravel Framework 7.9.2
  * MySQL в качестве базы данных (5.7.29)
  * NodeJS и npm для ассетов

## Инструкция по разворчиванию окружения
* **Первый вариант использовав docker**

    ```
    git clone https://gitlab.com/it.devel.att/laravel_help_desk.git
    
    cd laravel_help_desk
    
    bash start_demo.sh
    ```
    
    >  * Это запустит сборку приложения через docker-compose
    >  * После сборки докер образов запустится поднятие приложения
    >  * Прогонятся миграции
    >  * Прогонятся сиды **(Учитывая что есть базовые менеджеры, при создании сидов для остальных пользователей может возникнуть проблема с тем что Faker сгенерирует уже существующий email, в этом случае надо будет перезапустить сборку)**
    >  * Запустятся очереди **(Для email уведомлений)**
    >  * **Все загруженные пользователи и менеджеры появятся в файле users.txt в папке проекта**
    > ![alt text](https://i.ibb.co/LdhsDSD/104.png "Title Text")
    > * После успешной сборки и запуска приложение будет доступно на localhost 80 порту. **http://localhost/login**
    > * **Приложение в докере stateless. Для демо версии я не стал волюмить базу данных. Так проще перезапускать приложение с сидами**
* **Второй вариант локально, но он требует всех установленных пакетов и базу данных MySQL**
    * После клонирование приложения в папке с проектом надо будет создать создать файл .env для хранения конфигов. Основные параметры это:
>         APP_NAME=HelpDesk
>         APP_ENV=local
>         APP_KEY=**СЛУЧАЙНО_СГЕНЕРИРОВАНЫЙ_КЛЮЧ**
>         APP_DEBUG=true
>         APP_URL=http://localhost
>         
>         LOG_CHANNEL=stack
>         
>         DB_CONNECTION=mysql
>         DB_HOST=127.0.0.1
>         DB_PORT=3306
>         DB_DATABASE=**ВАШЕ_ИМЯ_БАЗЫ_ДАННЫХ**
>         DB_USERNAME=**ПОЛЬЗОВАТЕЛЬ_ЛОКАЛЬНОЙ_БАЗЫ_ДАННЫХ**
>         DB_PASSWORD=**ПАРОЛЬ_ЛОКАЛЬНОЙ_БАЗЫ_ДАННЫХ**
>         
>         BROADCAST_DRIVER=log
>         CACHE_DRIVER=file
>         QUEUE_CONNECTION=database
>         SESSION_DRIVER=file
>         SESSION_LIFETIME=120
>         
>         REDIS_HOST=127.0.0.1
>         REDIS_PASSWORD=null
>         REDIS_PORT=6379
>         
>         MAIL_MAILER=smtp
>         MAIL_HOST=smtp.gmail.com
>         MAIL_PORT=465
>         MAIL_USERNAME=**ПОЧТА_ОТ_ВАШЕГО_GMAIL_С_КОТОРОГО_БУДЕТ_РАССЫЛКА_НОТИФИКАЦИЙ**
>         MAIL_PASSWORD=**ПАРОЛЬ_ОТ_ПОЧТЫ**
>         MAIL_ENCRYPTION=ssl
>         MAIL_FROM_ADDRESS=**ПОЧТА_ОТ_ВАШЕГО_GMAIL_С_КОТОРОГО_БУДЕТ_РАССЫЛКА_НОТИФИКАЦИЙ**
>         MAIL_FROM_NAME="${APP_NAME}"
>         
>         AWS_ACCESS_KEY_ID=
>         AWS_SECRET_ACCESS_KEY=
>         AWS_DEFAULT_REGION=us-east-1
>         AWS_BUCKET=
>         
>         PUSHER_APP_ID=
>         PUSHER_APP_KEY=
>         PUSHER_APP_SECRET=
>         PUSHER_APP_CLUSTER=mt1
>         
>         MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
>         MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
    
* После создания .env файла в папке проекта прогнать команды

   ```
    git clone https://gitlab.com/it.devel.att/laravel_help_desk.git
    
    cd laravel_help_desk
    **Создание .env файла с конфигами**
    composer install
    npm install
    npm run dev
    php artisan migrate
    php artisan db:seed
    php artisan storage:link
    ```
    * После этого нужно запустить очередь сообщений и сам сервер. Нужно открыть два окна терминала в папке проекта и:
      * В одном запустить `php artisan serve`
      * Во втором запустить `php artisan queue:work database`
    * После этого приложение будет доступно локально на порту 8000. http://localhost:8000/login