<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'OrderController@index')->middleware('auth');

Route::group(['middleware' => ['auth', 'client']], function () {
    Route::prefix('orders')->group(function () {
        Route::get('/', 'OrderController@index');
        Route::get('/new', 'OrderController@new');
        Route::post('/create', 'OrderController@create');
        Route::get('/{id}', 'OrderController@show');
        Route::put('/{id}/closeOrder', 'OrderController@closeOrder');

        Route::prefix('{orderId}/answers')->group(function () {
            Route::post('/', 'AnswerController@create');
        });
    });
});

Route::namespace('Manager')->group(function () {
    Route::prefix('managers')->group(function () {
        Route::prefix('orders')->group(function () {
            Route::get('/', 'OrderController@index');
            Route::get('/{id}', 'OrderController@show');
            Route::put('/{id}/accept', 'OrderController@accept');

            Route::prefix('{orderId}/answers')->group(function () {
                Route::post('/', 'AnswerController@create');
            });
        });
    });
});


