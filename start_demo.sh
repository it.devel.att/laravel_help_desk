#!/bin/bash
set -e

TAG=demo docker-compose build
TAG=demo docker-compose up -d
RETRIES=10
echo "Mysql db initialize in docker container not very fast. Need to wait....."
#Use user and password for db like this UNSAFE. Just for demo!
until TAG=demo docker-compose exec -T helpdesk_db mysql --user=helpdesk_admin --password=lSw0nLVOt3BIWbPuQA9U -e "SHOW DATABASES LIKE 'helpdesk_best';"  > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Wait for Mysql will ready for accept connection, ${RETRIES} remaining attempts..."
  sleep 5
  RETRIES=$((RETRIES-1))
done
sleep 2
echo "Mysql Ready. Run migration"
TAG=demo docker-compose exec helpdesk_app php artisan migrate
echo "Fill database with seeds"
TAG=demo docker-compose exec helpdesk_app php artisan db:seed >> users.txt
echo "Start queue for email sends"
TAG=demo docker-compose exec -d helpdesk_app php artisan queue:work database
echo -e "Open \e[42musers.txt\e[49m file for get list of users and managers with password"
