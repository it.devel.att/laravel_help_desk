<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Order::class, function (Faker $faker) {
    $daysAgo = rand(1, 10);

    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => $faker->text($maxNbChars = 1000),
        'open' => [true, false][rand(0, 1)],
        'created_at' => Carbon::now()->subDays($daysAgo),
        'updated_at' => Carbon::now()->subDays($daysAgo),
    ];
});
