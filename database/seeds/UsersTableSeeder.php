<?php

use App\Answer;
use App\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $managerPassword = 'manager_password';
        $baseManager = $this->createBaseManager($managerPassword, 'manager@email.com');
        $secondBaseManager = $this->createBaseManager($managerPassword, 'second_manager@email.com');
        $thirdBaseManager = $this->createBaseManager($managerPassword, 'it.devel.att@gmail.com');

        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->orders()->saveMany(factory(App\Order::class, 10)->make());
            print 'Create employee with email: ' . $u->email . " \n";
        });

        $managers = [$baseManager->id, $secondBaseManager->id, null, null];
        $orders = Order::all();

        $this->addAnswersToSomeOrders($orders, $managers);


        print "Password for all user (client): 12345678 \n";

        $this->printInfoAboutManagers([$baseManager, $secondBaseManager], $managerPassword);
    }

    public function addAnswersToSomeOrders(Collection $orders, array $managers)
    {
        $leaveAnswerToOrderByManagerChance = [true, true, false];
        $faker = Faker::create();

        foreach ($orders as $order) {
            $order->manager_id = $managers[rand(0, count($managers) - 1)];
            $order->save();

            $leaveAnswer = $leaveAnswerToOrderByManagerChance[rand(0, count($leaveAnswerToOrderByManagerChance) - 1)];
            if ($order->manager_id != null and $leaveAnswer) {
                $answer = new Answer;
                $answer->order_id = $order->id;
                $answer->user_id = $order->manager_id;
                $answer->description = $faker->text($maxNbChars = 1000);
                $answer->save();
            }
        }
    }

    public function createBaseManager(string $managePassword, string $baseManagerEmail): App\User
    {
        $managerNames = ["David Manager", "Piter Manager", "John Manager", "Doe Manager"];
        $baseManager = new App\User;
        $baseManager->email = $baseManagerEmail;
        $baseManager->name = $managerNames[rand(0, count($managerNames) - 1)];
        $baseManager->password = bcrypt($managePassword);
        $baseManager->manager = true;
        $baseManager->client = false;
        $baseManager->save();
        return $baseManager;
    }

    public function printInfoAboutManagers(array $managers, string $managerPassword): void
    {
        foreach ($managers as $manager) {
            print "==========================\n";
            print "Base manager user: \n";
            print "Email: " . $manager->email . " \n";
            print "Password: " . $managerPassword . " \n";
        }

    }
}
