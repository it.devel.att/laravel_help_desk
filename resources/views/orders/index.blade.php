@extends('layouts.app')
@section('content')
    <h1>Orders</h1>
    <a href="{{url('orders/new')}}">Create Order</a>
    <hr>
    <div class="row">
        @foreach($orders as $order)
            <div class="col-12 shadow p-3 mb-4 rounded @if($order->open)bg-white @else closed-order @endif">
                <div class="d-flex justify-content-between">
                    <div>
                        <b>Title:</b> @if(strlen($order->title) > 122) {{substr($order->title, 0, 122) . "..."}} @else {{$order->title}} @endif
                    </div>
                    <div>
                        @if(!$order->open)
                            <p class="mb-0 mt-auto text-danger">Order closed</p>
                        @endif
                    </div>
                </div>
                <p>
                    <b>Description:</b> @if(strlen($order->description) > 122) {{substr($order->description, 0, 122) . "..."}} @else {{$order->description}} @endif
                </p>
                <div class="d-flex justify-content-between">
                    <div class="d-flex justify-content-between">
                        <a href="{{url('orders/'. $order->id)}}" class="btn btn-primary mr-1">Show</a>
                        @if($order->open)
                            <form action="{{url('orders/' . $order->id . '/closeOrder')}}" method="POST">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-warning">Close order</button>
                            </form>
                        @endif
                    </div>
                    <small class="text-muted mt-auto">{{$order->created_at}}</small>
                </div>
                @if($order->manager)
                    <p class="mb-0 mt-2 text-success">
                        This order accepted by manager <b>{{$order->manager->name}}</b>
                    </p>
                @endif
            </div>
        @endforeach
    </div>
    {{ $orders->links() }}
@endsection
