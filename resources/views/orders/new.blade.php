@extends('layouts.app')
@section('content')
<h5>Create new Order</h5>

<form action="{{url('orders/create')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="inputOrderTitle">Order Title:</label>
        <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="inputOrderTitle">
        @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="inputOrderDescription">Order Description</label>
        <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="inputOrderDescription" rows="10"></textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="custom-file mb-2">
        <input type="file" name="file" class="custom-file-input @error('file') is-invalid @enderror" id="validatedCustomFile">
        <label class="custom-file-label" for="validatedCustomFile">Attach file...</label>
        @error('file')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

