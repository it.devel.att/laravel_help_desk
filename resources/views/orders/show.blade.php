@extends('layouts.app')
@section('content')
    <div class="shadow p-4 mb-3">
        <h4>{{$order->title}}</h4>
        <p>{{$order->description}}</p>
        <small class="text-muted">{{$order->created_at}}</small>
    </div>
    @if($order->file)
        <a href="{{asset('storage/' . $order->file)}}" class="btn btn-outline-primary">Download attached file</a>
    @endif
    <hr>
    <h4>Answer:</h4>
    @if($order->open)
        <form action="{{ url('orders/' . $order->id . '/answers') }}" method="POST">
            @csrf
            <textarea name="description" class="form-control @error('description') is-invalid @enderror"
                      rows="5"></textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
            <button type="submit" class="btn btn-success my-3">Answer</button>
        </form>
    @else
        <div class="alert alert-danger">Cannot leave answer on closed order</div>
    @endif
    @foreach($answers as $answer)
        <div class="card shadow-sm mb-3 bg-white rounded">
            <div class="card-header d-flex justify-content-between">
                <p class="mb-0">{{$answer->user->name}} <b>{{$answer->user->email}}</b></p>
                <small class="mb-0 text-muted">{{$answer->created_at}}</small>
            </div>
            <div class="card-body">
                <p class="card-text">{{$answer->description}}</p>
            </div>
        </div>
    @endforeach
    {{ $answers->links() }}
@endsection
