@extends('layouts.app')
@section('content')
    <div class="d-flex justify-content-between">
        <h1>Orders</h1>
        <div class="dropleft">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{url('managers/orders')}}">All</a>
                <a class="dropdown-item" href="{{url('managers/orders?open=false')}}">Closed</a>
                <a class="dropdown-item" href="{{url('managers/orders?open=true')}}">Open</a>
                <a class="dropdown-item" href="{{url('managers/orders?accepted=true')}}">Accepted</a>
                <a class="dropdown-item" href="{{url('managers/orders?watched=true')}}">Read</a>
                <a class="dropdown-item" href="{{url('managers/orders?watched=false')}}">Not read</a>
                <a class="dropdown-item" href="{{url('managers/orders?withAnswer=true')}}">With answer</a>
                <a class="dropdown-item" href="{{url('managers/orders?withAnswer=false')}}">Without answer</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        @foreach($orders as $order)
            <div class="col-12 shadow p-3 mb-4 rounded {{ findStyleDependOnOrderStatus($order, Auth::user()) }}">
                <h3>
                    {{$order->user->name}}. <b>{{$order->user->email}}</b>
                </h3>
                <hr>
                <p>
                    <b>Title:</b> @if(strlen($order->title) > 122) {{substr($order->title, 0, 122) . "..."}} @else {{$order->title}} @endif
                </p>
                <p>
                    <b>Description:</b> @if(strlen($order->description) > 122) {{substr($order->description, 0, 122) . "..."}} @else {{$order->description}} @endif
                </p>
                <div class="d-flex justify-content-between">
                    <div class="d-flex justify-content-between">
                        <a href="{{url('managers/orders/'. $order->id)}}" class="btn btn-primary mr-1">Show</a>
                    </div>
                    <small class="text-muted mt-auto">{{$order->created_at}}</small>
                </div>
            </div>
        @endforeach
    </div>
    {{ $orders->appends($_GET)->links() }}
@endsection
