@extends('layouts.app')
@section('content')
    <div class="shadow p-4 mb-3">
        <h3>
            {{$order->user->name}}. <b>{{$order->user->email}}</b>
        </h3>
        <hr>
        <h4>{{$order->title}}</h4>
        <p>{{$order->description}}</p>
        <small class="text-muted">{{$order->created_at}}</small>
    </div>
    @if($order->file)
        <a href="{{asset('storage/' . $order->file)}}" class="btn btn-outline-primary">Download attached file</a>
    @endif
    <hr>
    @if($order->open)
        @if(!$order->manager)
            <div class="alert alert-warning"><b>Order don't have manager</b></div>
            <form action="{{url('managers/orders/' . $order->id . '/accept')}}" method="POST">
                @csrf
                @method('PUT')
                <button type="submit" class="btn btn-success mb-3">Accept order</button>
            </form>
        @elseif($order->manager->id != Auth::user()->id)
            <div class="alert alert-warning">This order accepted by <b>{{$order->manager->name}}</b></div>
            <h4>You cannot leave answer on order dont accepted by you</h4>
        @elseif($order->manager->id === Auth::user()->id)
            <div class="alert alert-success">This order belongs to you</div>
            <h4>Answer:</h4>
            <form action="{{ url('managers/orders/' . $order->id . '/answers') }}" method="POST">
                @csrf
                <textarea name="description" class="form-control" rows="5"></textarea>
                <button type="submit" class="btn btn-success my-3">Answer</button>
            </form>
        @endif
    @else
        <div class="alert alert-warning">
            Client <b>{{$order->user->name}}</b> close the order. You cannot leave answers on closed order.
        </div>
    @endif
    @foreach($answers as $answer)
        <div class="card shadow-sm mb-3 bg-white rounded">
            <div class="card-header d-flex justify-content-between">
                <p class="mb-0">{{$answer->user->name}} <b>{{$answer->user->email}}</b></p>
                <small class="mb-0 text-muted">{{$answer->created_at}}</small>
            </div>
            <div class="card-body">
                <p class="card-text">{{$answer->description}}</p>
            </div>
        </div>
    @endforeach
    {{ $answers->links() }}
@endsection
