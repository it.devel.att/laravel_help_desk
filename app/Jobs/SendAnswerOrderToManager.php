<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendAnswerOrderToManager implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $managerEmail;
    private $data;
    private $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $name, $managerEmail)
    {
        $this->data = $data;
        $this->name = $name;
        $this->managerEmail = $managerEmail;
    }


    public function handle()
    {
        $name = $this->name;
        $managerEmail = $this->managerEmail;
        $data = $this->data;

        Mail::send('emails.client_answer_on_order', $this->data, function ($message) use ($name, $managerEmail, $data) {
            $message->to($managerEmail, $name)
                ->subject('Client ' . $data['client']->name . ' answer on order.');
            $message->from('helpdesk.test.project@gmail.com', 'Client answer on order.');
        });
    }
}
