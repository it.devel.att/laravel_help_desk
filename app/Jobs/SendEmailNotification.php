<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $managerEmail;
    private $data;
    private $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $name, $managerEmail)
    {
        $this->data = $data;
        $this->name = $name;
        $this->managerEmail = $managerEmail;
    }


    public function handle()
    {
        $name = $this->name;
        $managerEmail = $this->managerEmail;
        $data = $this->data;

        Mail::send('emails.new_order', $this->data, function ($message) use ($name, $data, $managerEmail) {
            $message->to($managerEmail, $name)
                ->subject('Client ' . $data['client']->name . ' needs your help!');
            $message->from('helpdesk.test.project@gmail.com', 'New Order on HelpDesk');
        });
    }
}
