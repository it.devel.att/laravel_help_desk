<?php

namespace App\Http\Middleware;

use Closure;

class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->manager) {
            return redirect('/managers/orders')->with('accessError', 'You don\'t have access to client panel');
        }
        return $next($request);
    }
}
