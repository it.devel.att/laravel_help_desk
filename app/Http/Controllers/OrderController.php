<?php

namespace App\Http\Controllers;

use App\Jobs\SendCloseOrderToManager;
use App\Jobs\SendEmailNotification;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class OrderController extends Controller
{
    public function index()
    {
        if (Auth::user()->manager) {
            return redirect()->action('Manager\OrderController@index');
        }
        $orders = Auth::user()->orders()->orderBy('open', 'desc')->orderBy('created_at', 'desc')->paginate(10);
        return view('orders.index', ['orders' => $orders]);
    }

    public function new()
    {
        return view('orders.new');
    }

    public function show($id)
    {
        $order = Auth::user()->orders()->where('id', $id)->first();
        if (!$order) {
            return abort(404);
        }
        $answers = $order->answers()->orderBy('created_at', 'desc')->paginate(10);
        return view('orders.show', ['order' => $order, 'answers' => $answers]);
    }

    protected function create(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'min:10', 'max:255'],
            'description' => ['required', 'string', 'min:10', 'max:1000'],
            'file' => ['max:10000', 'mimes:pdf']
        ]);

        $lastOrder = Auth::user()->orders()->get()->last();
        $yesterday = Carbon::now()->subDays(1);
        if ($lastOrder and $lastOrder->created_at > $yesterday) {
            return redirect()->back()->with('accessError', 'You cannot create orders more than once a 24 hours');
        }

        $order = new Order;
        if (request()->file) {
            $fileName = Auth::user()->id . '_file' . time() . '.' . request()->file->getClientOriginalExtension();
            $request->file->storeAs('files', $fileName);
            $order->file = $fileName;
        }
        $order->title = $request->title;
        $order->description = $request->description;
        $order->client_id = Auth::user()->id;
        $order->save();

        $managers = User::where('manager', true)->get();
        $data = ['client' => $order->user, 'order' => $order];

        foreach ($managers as $manager) {
            $name = $manager->name;
            $managerEmail = $manager->email;

            dispatch(new SendEmailNotification($data, $name, $managerEmail));
        }

        return redirect()->action('OrderController@index');
    }

    protected function closeOrder($id)
    {
        $currentUser = Auth::user();
        $order = Order::where('client_id', $currentUser->id)->where('id', $id)->first();
        if (!$order) {
            return abort(404);
        }
        $order->open = false;
        $order->save();

        $managerOfOrder = $order->manager;
        if ($managerOfOrder) {
            $data = ['client' => $currentUser, 'order' => $order];
            $name = $managerOfOrder->name;
            $managerEmail = $managerOfOrder->email;

            dispatch(new SendCloseOrderToManager($data, $name, $managerEmail));
        }

        return redirect()->back();
    }
}
