<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;


class Manager extends Controller
{
    public function __construct()
    {
        $this->middleware('manager');
    }
}
