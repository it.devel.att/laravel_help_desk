<?php

namespace App\Http\Controllers\Manager;

use App\Answer;
use App\Jobs\SendEmailToClientAboutManagerAnswer;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Manager
{
    protected function create(Request $request, string $orderId)
    {
        $this->validate($request, [
            'description' => ['required', 'string', 'min:10', 'max:1000']
        ]);
        $order = Order::find($orderId);
        if (!$order) {
            return abort(404);
        } elseif (!$order->manager) {
            return redirect()->back()->with('accessError', 'First you need to accept order');
        } elseif ($order->manager->id !== Auth::user()->id) {
            return redirect()->back()->with('accessError', 'You cannot answer on order dont accepted by you');
        } elseif (!$order->open) {
            return redirect()->back()->with('accessError', 'You cannot leave answer on closed order');
        }
        $answer = new Answer;
        $answer->description = $request->description;
        $answer->order_id = $orderId;
        $answer->user_id = Auth::user()->id;
        $answer->save();

        $clientOfOrder = $order->user;
        $data = ['client' => $clientOfOrder, 'answer' => $answer];
        $name = $clientOfOrder->name;
        $clientEmail = $clientOfOrder->email;

        dispatch(new SendEmailToClientAboutManagerAnswer($data, $name, $clientEmail));

        return redirect()->back();
    }
}
