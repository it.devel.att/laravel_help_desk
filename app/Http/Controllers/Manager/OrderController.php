<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Manager\Manager;
use App\ManagerOrderView;
use App\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Manager
{
    public function index(Request $request)
    {
        if (!empty($request->all())) {
            $orders = $this->ordersByQueryParams($request);
        } else {
            $orders = Order::with('user')
                ->orderBy('open', 'desc')->orderBy('created_at', 'desc')->paginate(10);
        }
        return view('managers.orders.index', ['orders' => $orders]);
    }

    public function show($id)
    {
        $order = Order::find($id);
        $answers = $order->answers()->with('user')->orderBy('created_at', 'desc')->paginate(10);
        if (!$order) {
            return abort(404);
        }
        $countViews = ManagerOrderView::where('order_id', $order->id)->where('manager_id', Auth::user()->id)->first();
        if ($countViews) {
            $countViews->count += 1;
            $countViews->save();
        } else {
            $countViews = new ManagerOrderView;
            $countViews->order_id = $order->id;
            $countViews->manager_id = Auth::user()->id;
            $countViews->count = 1;
            $countViews->save();
        }
        return view('managers.orders.show', ['order' => $order, 'answers' => $answers]);
    }

    protected function accept($id)
    {
        $order = Order::find($id);
        if (!$order) {
            return abort(404);
        } elseif ($order->manager) {
            return redirect()->back()->with('accessError', 'This Order already accepted by ' . $order->manager->name);
        }
        $order->manager_id = Auth::user()->id;
        $order->save();
        return redirect()->back()->with('success', 'You successfully assigned yourself on order.');
    }

    private function ordersByQueryParams(Request $request)
    {
        if ($request->query('open') == "true") {
            return Order::open()->paginate(10);
        } elseif ($request->query('open') == "false") {
            return Order::closed()->paginate(10);
        } elseif ($request->query('accepted') == "true") {
            return Order::acceptedByCurrentManager()->paginate(10);
        } elseif ($request->query('watched') == "true") {
            return Order::read()->paginate(10);
        } elseif ($request->query('watched') == "false") {
            return Order::unread()->paginate(10);
        } elseif ($request->query('withAnswer') == "true") {
            return Order::withAnswer()->paginate(10);
        } elseif ($request->query('withAnswer') == "false") {
            return Order::withOutAnswer()->paginate(10);
        } else {
            return Order::with('user')
                ->orderBy('open', 'desc')
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        }
    }
}
