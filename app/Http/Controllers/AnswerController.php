<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Jobs\SendAnswerOrderToManager;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    protected function create(Request $request, string $orderId)
    {
        $this->validate($request, [
            'description' => ['required', 'string', 'min:10', 'max:1000']
        ]);
        $order = Order::find($orderId);
        if (!$order) {
            return abort(404);
        } elseif ($order->user->id !== Auth::user()->id) {
            return redirect()->back()->with('accessError', 'You cannot leave answer on order dont belongs to you');
        } elseif (!$order->open) {
            return redirect()->back()->with('accessError', 'You cannot leave answer on closed order');
        }
        $answer = new Answer;
        $answer->description = $request->description;
        $answer->order_id = (int) $orderId;
        $answer->user_id = Auth::user()->id;
        $answer->save();

        $managerOfOrder = $order->manager;
        if ($managerOfOrder) {
            $data = ['client' => $answer->user, 'answer' => $answer];
            $name = $managerOfOrder->name;
            $managerEmail = $managerOfOrder->email;

            dispatch(new SendAnswerOrderToManager($data, $name, $managerEmail));
        }

        return redirect()->back();
    }
}
