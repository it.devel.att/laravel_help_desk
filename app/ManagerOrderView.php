<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ManagerOrderView extends Model
{

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
