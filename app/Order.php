<?php

namespace App;

use App\Answer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    private $title;
    private $description;
    private $client_id;
    private $file;
    private $user;

    public function user()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'order_id');
    }

    public function manager_order_views()
    {
        return $this->hasMany(ManagerOrderView::class, 'order_id');
    }

    public static function open()
    {
        return Order::with('user')->where('open', true)->orderBy('created_at', 'desc');
    }

    public static function closed()
    {
        return Order::with('user')->where('open', false)->orderBy('created_at', 'desc');
    }

    public static function acceptedByCurrentManager()
    {
        return Order::with('user')
            ->where('manager_id', Auth::user()->id)
            ->orderBy('open', 'desc')->orderBy('created_at', 'desc');
    }

    public static function read()
    {
        return Order::with('user')->whereHas('manager_order_views', function (Builder $query) {
            $query->where('manager_id', Auth::user()->id);
        });
    }

    public static function unread()
    {
        return Order::with('user')
            ->whereHas('manager_order_views', function (Builder $query) {
                $query->where('manager_id', '!=', Auth::user()->id);
            })
            ->orDoesntHave('manager_order_views')
            ->orderBy('open', 'desc')
            ->orderBy('created_at', 'desc');
    }

    public static function withAnswer()
    {
        return Order::with('user')
            ->whereHas('answers')
            ->orderBy('open', 'desc')
            ->orderBy('created_at', 'desc');
    }

    public static function withOutAnswer()
    {
        return Order::with('user')
            ->whereDoesntHave('answers')
            ->orderBy('open', 'desc')
            ->orderBy('created_at', 'desc');
    }
}
