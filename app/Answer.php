<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    private $user;
    private $order;
    private $user_id;
    private $order_id;
    private $description;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function manager()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
