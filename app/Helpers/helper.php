<?php
function findStyleDependOnOrderStatus($order, $manager){
    $style = "";
    if ($order->open){
        if (!$order->manager){
            $style .= " bg-white";
        } elseif ($order->manager->id === $manager->id){
            $style .= " order-belongs-to-current-manager";
        } elseif ($order->manager->id !== $manager->id){
            $style .= " order-belongs-to-another-manager";
        }
    } else {
        $style .= " closed-order";
    }
    return $style;
}
